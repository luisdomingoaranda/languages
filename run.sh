sentences2anki \
  --text2speech_api google \
  --credentials './gcloud_service_account_credentials.json' \
  --without_translation_cards \
  --file $1
  # --anki_collection '/home/luis/.local/share/Anki2/User 1' \

# sentences2anki \
#   --text2speech_api openai \
#   --env_file ./.env \
#   --without_translation_cards \
#   --file $1 \
#   # --anki_collection '/home/luis/.local/share/Anki2/User 1' \

# sentences2anki \
#   --text2speech_api elevenlabs \
#   --env_file ./.env \
#   --voices_json ./elevenlabs_voices.json \
#   --without_translation_cards \
#   --file $1
#   # --anki_collection '/home/username/.local/share/Anki2/User 1'

# sentences2anki \
#   --text2speech_api any \
#   --credentials './gcloud_service_account_credentials.json' \
#   --env_file ./.env \
#   --voices_json ./elevenlabs_voices.json \
#   --without_translation_cards \
#   --file $1
#   # --anki_collection '/home/username/.local/share/Anki2/User 1'
